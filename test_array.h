//////////////////////////////////////////////////////////////////////////////
/// @file test_array.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header for the Test Array class, 
/// which does unit testing on the Array class.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class Test_array
/// @brief Design to unit test the Array class.
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST_SUITE (Test_array);
/// @brief This calls the unit testing library for CPP, 
/// using this header file as an argument
/// @pre Requires the indicated header file to exist 
/// @post Reports the success or failure of unit tests.
/// @param Test_array is the header being called
/// @return I believe this is void, instead cout-ing 
/// the results to the terminal
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_push_back);
/// @brief This calls the unit testing library for CPP, 
/// using the test_push_back member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_push_back is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_scope);
/// @brief This calls the unit testing library for CPP, 
/// using the test_scope member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_scope is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn CPPUNIT_TEST (test_pop_back);
/// @brief This calls the unit testing library for CPP, 
/// using the test_pop_back member function as an argument
/// @pre Requires the indicated member function to exist 
/// @post Reports the success or failure of unit test.
/// @param test_pop_back is the function being called
/// @return True/false based upon success or failure of test.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn test_push_back
/// @brief This tests the push_back function of the Array class
/// @pre Array class should already be declared. 
/// This will instantiate one and test the push_back function.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn test_scope
/// @brief This tests the scope of the Array class, making sure
/// that you can't just read and write outside of the existing array.
/// @pre Of course, you can't test the scope of the array without having 
/// something in it.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn test_pop_back
/// @brief This tests the pop_back function of the Array class
/// @pre I have to use push_back to fill this array for testing of the 
/// pop_back function.
/// @post All the changes made by this function are handled by 
/// the parent CPPUNIT_TEST function, as reported back by 
/// CPP_UNIT_ASSERT statements.
/// @param None.
/// @return None.
//////////////////////////////////////////////////////////////////////////////


#ifndef TEST_ARRAY_H
#define TEST_ARRAY_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "array.h"

class Test_array : public CPPUNIT_NS::TestFixture
{
  private:
    CPPUNIT_TEST_SUITE (Test_array);
      CPPUNIT_TEST (test_push_back);
      CPPUNIT_TEST (test_scope);
      CPPUNIT_TEST (test_pop_back);
    CPPUNIT_TEST_SUITE_END ();

  protected:
    void test_push_back ();
    void test_scope (); 
    void test_pop_back ();
};

#endif 
