//////////////////////////////////////////////////////////////////////////////
/// @file array.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the 
///  header of the Array class.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
/// @fn Array()
/// @brief Default constructor for Array
/// @pre Array does not exist
/// @post An array of max size 20 and a current size of zero exists
/// @param None
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////
template <class generic>
Array<generic>::Array () : m_size (0), m_max_size (20)
{
}


//////////////////////////////////////////////////////////////////////
/// @fn push_back(generic x)
/// @brief Adds the parameter to the end of the array, 
/// unless the array is full.  
/// @pre Array exists   
/// @post Array either has one more element in it (and has size increased
/// by  one), or it throws an exception. 
/// @param Element to be inserted into the array                                  
/// @return None (void)      
//////////////////////////////////////////////////////////////////////
template <class generic>
void Array<generic>::push_back (generic x)
{
  if(m_size >= m_max_size)//Attempting to increase beyond max size
  {
    throw Exception
    (
      CONTAINER_FULL,
      "The container is full and no more items can be added."
    );
  }
  m_data[m_size++] = x;
}

//////////////////////////////////////////////////////////////////////
/// @fn pop_back()
/// @brief Reduces size by one, unless size is less than or equal to zero,
/// in which case it throws an exception. This has the effect of making the 
/// last element in the array unable to be read or written to, unless 
/// push_back is called, which would overwrite the now orphaned element.
/// @pre Array exists   
/// @post Array either has size decreased by one, or it throws an exception. 
/// @param None
/// @return None (void)      
//////////////////////////////////////////////////////////////////////
template <class generic>
void Array<generic>::pop_back ()
{
  if(m_size <= 0)//Attempting to make size negative.
  {
    throw Exception
    (
      CONTAINER_EMPTY,
      "The conatiner is empty and no more items can be removed."
    );
  }
  m_size--;
}

//////////////////////////////////////////////////////////////////////
/// @fn clear()
/// @brief Reduces size to zero. 
///This has the effect of making all elements in the array unable to be read
/// or written to, unless push_back is called, which would overwrite the 
/// now orphaned elements.
/// @pre Array exists   
/// @post Array size now is zero 
/// @param None
/// @return None (void)      
//////////////////////////////////////////////////////////////////////
template <class generic>
void Array<generic>::clear ()
{
  m_size = 0;
}

template <class generic>
generic& Array<generic>::operator[] (unsigned int x)
{
  if((x >= m_size) || (x < 0)) //Trying to write out of bounds
  {
    throw Exception
    (
      OUT_OF_BOUNDS,
      "You are attempting to write outside the current scope of the array."
    ); 
  }
  
  return m_data[x];
}

template <class generic>
generic& Array<generic>::operator[] (unsigned int x) const
{
               
   if((x >= m_size) || (x < 0)) //Trying to read out of bounds
   {
    throw Exception
    (
      OUT_OF_BOUNDS,
      "You are attempting to read outside the current scope of the array."
    );
  }

  return m_data[x];
}

//////////////////////////////////////////////////////////////////////
/// @fn size()
/// @brief Gets current size of the array. 
/// @pre Array exists   
/// @post Returns Array Size
/// @param None
/// @return Size of the array as an integer.      
//////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int Array<generic>::size () const
{
  return m_size;
}

//////////////////////////////////////////////////////////////////////
/// @fn max_size()
/// @brief Gets the max size of the array. 
/// @pre Array exists   
/// @post Returns the maximum size of the array
/// @param None
/// @return Maximum size of the array as an integer.      
//////////////////////////////////////////////////////////////////////
template <class generic>
unsigned int Array<generic>::max_size () const
{
  return m_max_size;
}

