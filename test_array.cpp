//////////////////////////////////////////////////////////////////////////////
/// @file test_array.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the header of the 
/// test_array class, which performs unit testing on the Array class.
//////////////////////////////////////////////////////////////////////////////

#include "test_array.h"
#include <iostream>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_array);

//////////////////////////////////////////////////////////////////////
/// @fn test_push_back
/// @brief Tests the push_back function for correctness.
/// @pre This assumes the array class is implemented 
/// -- gotta test something
/// @post A couple of exceptions thrown, but this passes its results
/// to the CPPUNIT_TEST function
/// @param None. 
/// @return None. (void)
//////////////////////////////////////////////////////////////////////
void Test_array::test_push_back ()
{
  const int TEST_MAX = 20;

  //The following block of code shamelessly stolen from 
  //http://web.mst.edu/~buechler/DataStructures/InClassAssignments/sol/hw1/2010SP.pdf
  Array<int> a;
  
  cout << endl << "Push one item into the bag" << endl;
  a.push_back (3);
  CPPUNIT_ASSERT (a[0] == 3);
  CPPUNIT_ASSERT (a.size () == 1);
  CPPUNIT_ASSERT (a.max_size () == TEST_MAX);
  
  cout << "Push multiple items into the bag" << endl;
  for (int i = a.size (); i < TEST_MAX; i++)
  {
    a.push_back (i);
    CPPUNIT_ASSERT (a[i] == i);
    CPPUNIT_ASSERT (a.size () == i+1);
  }

  try 
  {
    cout << "Push overflow exception check" << endl;
    a.push_back (100);
    CPPUNIT_ASSERT (false); 
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_FULL == e.error_code ());
  }
}


//////////////////////////////////////////////////////////////////////
/// @fn test_pop_back
/// @brief This tests the pop_back function for correctness.
/// @pre Requires the push_back function to create the array.
/// @post All handled by the CPPUNIT_TEST
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////
void Test_array::test_pop_back ()
{
  cout << endl << "Prepping array..." << endl;
  Array<int> a;
  
  for(int i = 0; i < a.max_size () ; i++)
  {
    a.push_back(i);
  }
  cout << "Array Prepped. " << endl;

  cout << "Pop back one item." << endl;
  int temp = a.size();
  a.pop_back ();
  CPPUNIT_ASSERT ((temp - 1) == a.size());

  cout << "Pop back to beginning of container." << endl;
  for (int i = a.size(); i > 0; i--)
  {
    temp = a.size();
    a.pop_back ();
    CPPUNIT_ASSERT ((temp - 1) == a.size());
  }

  try
  {
    cout << "Pop back exception test." << endl;
    a.pop_back();
    CPPUNIT_ASSERT (false); 
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (CONTAINER_EMPTY == e.error_code ());
  }
}

//////////////////////////////////////////////////////////////////////
/// @fn test_scope
/// @brief Tests the exception handling for trying to walk off the 
/// array
/// @pre Need push_back to implement this.
/// @post All handled by the CPPUNIT_TEST
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////
void Test_array::test_scope ()
{
  cout << endl << "Prepping array..." << endl;
  Array<int> a;

  for(int i = 0; i < 10; i++)
  {
    a.push_back(i);
  }
  cout << "Array Prepped." << endl;

  cout << "Testing inside scope..." << endl;
  //Read
  CPPUNIT_ASSERT (a[3] == 3); 

  //Write
  a[3] = 4;
  CPPUNIT_ASSERT (a[3] == 4);
  
  int temp;
  try //Read negative
  {
    temp = a[-1];
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }
  
  try //Read above size
  {
    temp = a[a.size()+1];
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }


  temp = 3;  
  try //Write negative
  {
    a[-1] = temp;
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }

  try //Write above size
  {
    a[a.size()+1] = temp;
    CPPUNIT_ASSERT (0);
  }
  catch (Exception & e)
  {
    CPPUNIT_ASSERT (OUT_OF_BOUNDS == e.error_code ());
  }

  cout <<  "Scope test passed." << endl;
  
  return; //void
}
