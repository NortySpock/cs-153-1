//////////////////////////////////////////////////////////////////////////////
/// @file array.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header for the Array class
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class Array
/// @brief A class to improve on the current implementation 
/// of the array. It does error handling and catches exceptions -- 
/// more than the current array does.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn Array ()
/// @brief Default constructor for array
/// @pre Array does not exist
/// @post Array exists
/// @param No parameters.
/// @return None. (constructor)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void push_back (generic)
/// @brief This adds another element to end of the array, with built in error 
/// and exception-handling incase you try to walk off the end of the array.
/// @pre Array exists
/// @post Array either has one more element in it, or throws an exception
/// if this would walk off the end of the array.
/// Size of array is increased by one.
/// @param Value to be placed in the array.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void pop_back (generic)
/// @brief This removes an element from the end the array, with built in error 
/// and exception-handling in case you try to walk off the end of the array.
/// @pre Array exists
/// @post Size of array is decreased by one, or throws an exception
/// if this would walk off the end of the array.
/// Note that this does not actually delete the element that was at the end of
/// the array -- it just declares that the array is now one element smaller, so
/// you shouldn't be able to access it.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn void clear ()
/// @brief This sets the size of the array to zero. Note that this does not
/// delete the values of the elements in the array, it just makes them all 
/// unavailable and able only to be overwritten. 
/// @pre Array exists
/// @post  Size of array is set to zero.
/// @param None.
/// @return None. (void)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int size ()
/// @brief Gets the current size of the array.
/// @pre Array exists
/// @post No change                                           
/// @param None.
/// @return Current size of the array (zero for no elements in the array, one
/// for one element currently in the array, etc.)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn unsigned int max_size ()
/// @brief Gets the max size of the array.                             
/// @pre Array exists  
/// @post No change                                                                
/// @param None.
/// @return Maximum size of the array 
//////////////////////////////////////////////////////////////////////////////

#ifndef ARRAY_H
#define ARRAY_H

#include "exception.h"

template <class generic>
class Array
{
  public:
    Array ();
    void push_back (generic);
    void pop_back ();
    void clear ();
    generic& operator[] (unsigned int);
    generic& operator[] (unsigned int) const;
    unsigned int size () const;
    unsigned int max_size () const;

  private:
    unsigned int m_size; /// < The current size of the array
    unsigned int m_max_size; /// < The maximum size of the array
    generic m_data[20]; /// < The actual array
};

#include "array.hpp"
#endif
